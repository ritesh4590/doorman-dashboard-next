import axios from "axios";

var CryptoJS = require("crypto-js");

const defaultOptions = {
	baseURL: "http://34.87.184.92/v1/",
	headers: {
		"Content-Type": "multipart/form-data",
	},
};

let instance = axios.create(defaultOptions);


instance.interceptors.request.use(function (config) {
	const encrypted_token = localStorage.getItem("token");
	
	if (encrypted_token) {
		var bytes = CryptoJS.AES.decrypt(encrypted_token, "process.env.FLEETZI_SECRET");
		var decrypted_token = bytes.toString(CryptoJS.enc.Utf8);
		config.headers.Authorization = `Token ${decrypted_token}`;	
	}
	return config;
});

const request = function (options) {
	const onSuccess = function (response) {
		console.debug("Request Successful!", response);
		return response.data;
	};

	const onError = function (error) {
		console.error("Request Failed:", error.config);

		if (error.response) {
			console.error("Status:", error.response.status);
			console.error("Data:", error.response.data);
			console.error("Headers:", error.response.headers);
		} else {
			console.error("Error Message:", error.message);
		}

		return Promise.reject(error.response || error.message);
	};

	return instance(options).then(onSuccess).catch(onError);
};

export default request;
