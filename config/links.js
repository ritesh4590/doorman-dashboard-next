import Link from "next/link";

export const BuildingLink = (props) => (
  <Link href={`/buildings`}>
    {props.children}
  </Link>
);

export const BuildingDetailLink = (props) => (
  <Link href={"/buildings/[uuid]"} as={`/buildings/${props.uuid}`}>
    {props.children}
  </Link>
);

export const BuildingResidenceLink = (props) => (
    <Link href="/buildings/[uuid]/residences" as={`/buildings/${props.uuid}/residences`}>
      {props.children}
    </Link>
);

export const ResidenceDetailLink = (props) => (
    <Link href="/buildings/[uuid]/residences/[residence_uuid]" as={`/buildings/${props.uuid}/residences/${props.residence_uuid}`}>
      {props.children}
    </Link>
);

export default BuildingLink;
