import React from "react";
import BrandLogo from "../svg/BrandLogo";
import "antd/dist/antd.css";
import Buildings from "../../config/links";
import Dashboard from "../../config/links";
import "./index.css";
import {
	Layout,
	Menu,
	Button,
	Input,
} from "antd";
import {
	DashboardOutlined,
	UserAddOutlined,
	BankOutlined
} from "@ant-design/icons";

import Router from "next/router";
import Link from "next/link";

const { Header, Content, Sider } = Layout;

const { Search } = Input;

export default class index extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			collapsed: false,
			key: "1"
		};
	}

	componentDidMount() {
		const encrypted_token = window.localStorage.getItem("token");
		if (!encrypted_token) {
			Router.push("/");
		}
	}

	logout() {
		localStorage.clear();
		window.location.href = "/";
	}

	onCollapse = (collapsed) => {
		console.log(collapsed);
		this.setState({ collapsed });
	};


	render() {
		return (
			<Layout style={{ minHeight: "100vh" }}>
				<Sider
					collapsible
					collapsed={this.state.collapsed}
					onCollapse={this.onCollapse}
				>
					{
						!(this.state.collapsed) ?
							<div style={{ textAlign: 'center', overflow: "hidden" }}>
								<BrandLogo width={140} height={54} />
							</div>
							:
							<div style={{ textAlign: 'center' }}>
								<BrandLogo width={60} height={54} />
							</div>
					}

					<Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
						<Menu.Item key="1">
							<DashboardOutlined />
							<Link href="/dashboard">
								<span>Dashboard</span>
							</Link>
						</Menu.Item>

						<Menu.Item key="2">
							<UserAddOutlined />
							<Link href="/complex-management">
								<span>User</span>
							</Link>
						</Menu.Item>

						<Menu.Item key="3">
							<UserAddOutlined />
							<Link href="/admin-land">
								<span>Admin Land</span>
							</Link>
						</Menu.Item>
						<Menu.Item key="4">
							<BankOutlined />
							<Buildings>
								<span>Buildings</span>
							</Buildings>
						</Menu.Item>
					</Menu>
				</Sider>
				<Layout className="site-layout">
					{/* <Header className="site-layout-background" style={{ padding: 0 }}>
						<Menu mode="horizontal">
							<Menu.Item key="1">
								<Search
									placeholder="Search"
									style={{ width: 200 }}
									onSearch={(value) => console.log(value)}
									style={{ marginTop: "15px", marginLeft: "50px" }}

								/>
							</Menu.Item>
							<Menu.Item key="2" style={{ float: "right" }}>
								<Button type="primary" onClick={() => this.logout()}>Logout</Button>
							</Menu.Item>
						</Menu>
					</Header> */}
					<Header className="site-layout-background" style={{ padding: 0 }} >
						<Menu mode="horizontal">
							<Menu.Item key="2" style={{ float: "right" }}>
								<Button type="primary" onClick={() => this.logout()}>Logout</Button>
							</Menu.Item>
						</Menu>
					</Header>
					<br />
					<Content style={{ margin: "20px 16px", }}>
						{this.props.children}
					</Content>
				</Layout>
			</Layout >
		);
	}
}
