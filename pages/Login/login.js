import React, { Component } from "react";
import Router from "next/router";
import { Button, notification } from 'antd';
import 'antd/dist/antd.css'
import BrandLogo from '../../components/svg/BrandLogo'
import './styles.css'
import request from "../../config/api.js";
import { ExclamationOutlined, CheckOutlined } from '@ant-design/icons';


var CryptoJS = require("crypto-js");


class Login extends Component {
	constructor(props) {
		// toast.configure();
		super(props);
		this.state = {
			phone_number: "",
			country_code: "+91",
			resendotp: false,
			submitButton: false,
			hash: "",
			phoneotp: "",
			otpsend: false,
		};
	}

	handleFormSubmit(event) {
		event.preventDefault();
	}

	openNotification = (status, message) => {
		notification.open({
			message: status.charAt(0).toUpperCase() + status.substring(1),
			description: message.charAt(0).toUpperCase() + message.substring(1),
			icon: (status === "success" ? <CheckOutlined style={{ color: 'green' }} /> : <ExclamationOutlined style={{ color: 'red' }} />)
		})
	};



	get_otp() {
		let formdata = new FormData();
		formdata.append("country_code", this.state.country_code);
		formdata.append("number", this.state.phone_number);
		formdata.append("platform", "part");
		request({
			method: "post",
			data: formdata,
			url: "/token/login/otp/request-sms/",
		})
			.then((resp) => {
				this.openNotification(resp.status, resp.message);
				if (resp.status === "success") {
					this.setState({
						hash: resp.data.hash,
						submitButton: true,
						otpsend: true,
					});
				}
			})
			.catch((e) => {
				this.openNotification(e.data.status, e.data.message);
				console.log(e);
			});
	}

	_verifyOTP() {
		let formdata = new FormData();
		formdata.append("country_code", this.state.country_code);
		formdata.append("number", this.state.phone_number);
		formdata.append("hash", this.state.hash);
		formdata.append("otp", this.state.phoneotp);
		request({
			method: "post",
			data: formdata,
			url: "/token/login/otp/verify/",
		})
			.then((resp) => {
				this.openNotification(resp.status, resp.message);
				if (resp.status === "success") {
					var ciphertext = CryptoJS.AES.encrypt(resp.data.access_token, "process.env.FLEETZI_SECRET");
					window.localStorage.setItem("token", ciphertext.toString());
					Router.push("/dashboard");
				}
			})
			.catch((e) => {
				console.log(e);
				this.openNotification(e.data.status, e.data.message);
			});
	};


	render() {
		return (
			<div className="body-container">
				<div className="transpatent-bg">
					<div className="company-brand"><BrandLogo height={200} width={200} /></div>
					<div className="login-form">
						<form className="login100-form validate-form">
							<span className="login100-form-title p-b-33">
							</span>
							<div className="wrap-input100 validate-input">
								<input
									className="input-form"
									type="text"
									required
									value={this.state.phone_number}
									onChange={(e) =>
										this.setState({ phone_number: e.target.value })
									}
									placeholder="Phone number"
								/>
							</div>
							<br />
							{this.state.otpsend ? (
								<div className="wrap-input100 validate-input">
									<input
										className="input-form"
										type="text"
										value={this.state.phoneotp}
										onChange={(e) => this.setState({ phoneotp: e.target.value })}
										placeholder="OTP"
										name="otp"
									/>
								</div>
							) : (
									<div></div>
								)}

							{this.state.hasError ? (
								<p style={{ paddingTop: 10, color: "red" }}>
									{this.state.errorText}
								</p>
							) : (
									<p></p>
								)}

							{this.state.submitButton ? (
								<div className="btn-container">
									<Button
										onClick={() => this._verifyOTP()}
										type="primary"
									>
										Login
                </Button>
								</div>
							) : (
									<div className="btn-container">
										<Button
											onClick={() => this.get_otp()}
											// className="login100-form-btn"
											type="primary"
										>
											Next
                </Button>
									</div>
								)}

						</form>
					</div>
				</div>
			</div>
		);
	}
}

export default Login;

