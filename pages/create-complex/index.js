import React, { Component } from "react";
import "./index.css";
import request from "../../config/api";
import Router from "next/router";
import Layout from "../../components/layout/index";
import { Form, Input, Button, notification, Row, Col, Typography } from "antd";
import { ExclamationOutlined, CheckOutlined } from "@ant-design/icons";

const { Title } = Typography;

export default class CreateComplex extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      contact_number: "",
      email: "",
      country: "",
      state: "",
      city: "",
      zipcode: "",
      longitude: "",
      latitude: "",
      address_line_1: "",
      address_line_2: "",
      landmark: "",
      Get_country: [],
      Get_state: [],
      Get_city: [],
      Get_zipcode: [],
      uuid: "",
    };
  }

  

  componentDidMount() {
    this.getCountries();

  }

  openNotification = (status, message) => {
    notification.open({
      message: status.charAt(0).toUpperCase() + status.substring(1),
      description: message.charAt(0).toUpperCase() + message.substring(1),
      icon:
        status === "success" ? (
          <CheckOutlined style={{ color: "green" }} />
        ) : (
          <ExclamationOutlined style={{ color: "red" }} />
        ),
    });
  };

  getCountries = () => {
    request({
      method: "get",
      url: "config/country",
    })
      .then((resp) => {
        if (resp.status === "success") {
          this.setState({
            Get_country: resp.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  getStates = (value) => {
    request({
      method: "get",
      url: "/config/country/" + value,
    })
      .then((resp) => {
        console.log("state---", resp);
        if (resp.status === "success") {
          this.setState({
            Get_state: resp.data.states,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  getCity = (value) => {
    request({
      method: "get",
      url: "/config/state/" + value,
    })
      .then((resp) => {
        if (resp.status === "success") {
          this.setState({
            Get_city: resp.data.cities,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  getZipCode = (value) => {
    request({
      method: "get",
      url: "/config/city/" + value,
    })
      .then((resp) => {
        if (resp.status === "success") {
          this.setState({
            Get_zipcode: resp.data.zipcode,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  handleFormSubmit(event) {
    event.preventDefault();
  }

  UpdateState = (value, type) => {
    if (type === "country") {
      this.setState({ country: value });
      this.getStates(value);
    }
    if (type === "state") {
      this.setState({ state: value });
      this.getCity(value);
    }
    if (type === "city") {
      this.setState({ city: value });
      this.getZipCode(value);
    }
    if (type === "zipcode") {
      this.setState({ zipcode: value });
    }
  };

  onSubmitComplex = () => {
    let formdata = new FormData();
    formdata.append("name", this.state.name);
    formdata.append("contact_number", this.state.contact_number);
    formdata.append("email", this.state.email);
    formdata.append("country", this.state.country);
    formdata.append("state", this.state.state);
    formdata.append("city", this.state.city);
    formdata.append("zipcode", this.state.zipcode);
    formdata.append("address_line_2", this.state.address_line_2);
    formdata.append("address_line_1", this.state.address_line_1);
    formdata.append("landmark", this.state.landmark);
    formdata.append("latitude", this.state.latitude);
    formdata.append("longitude", this.state.longitude);
    request({
      method: "post",
      data: formdata,
      url: "management/complex/",
    })
      .then((resp) => {
        this.openNotification(resp.status, resp.message);
        if (resp.status === "success") {
          Router.push("/dashboard");
          const { address } = this.state;
          address.push(resp.data);
          this.setState({
            address,
            visible: false,
          });
        }
      })
      .catch((e) => {
        console.log(e);
        // this.openNotification(e.data.status, e.data.message);
      });
  };

  render() {
    return (
      <>
        <Layout>
          <Row gutter={24} style={{ alignItems: "center" }}>
            <Col span={24}>
              <Title level={4}>Add Complex</Title>
            </Col>
          </Row>
          <Row style={{ marginTop: "20px" }}>
            <Col span={24}>
              <Form className="ant-advanced-search-form">
                <Row gutter={24}>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.name}
                      onChange={(e) => this.setState({ name: e.target.value })}
                    >
                      <Input placeholder="Complex Name" />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.contact_number}
                      onChange={(e) =>
                        this.setState({ contact_number: e.target.value })
                      }
                    >
                      <Input placeholder="Phone" />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.email}
                      onChange={(e) => this.setState({ email: e.target.value })}
                    >
                      <Input placeholder="Email" />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.address_line_1}
                      onChange={(e) =>
                        this.setState({ address_line_1: e.target.value })
                      }
                    >
                      <Input placeholder="Address Line " />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.address_line_2}
                      onChange={(e) =>
                        this.setState({ address_line_2: e.target.value })
                      }
                    >
                      <Input placeholder="Address Line 2" />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.country}
                      onChange={(e) =>
                        this.setState({ country: e.target.value })
                      }
                    >
                      <select
                        className="dropdown"
                        selectedvalue={this.state.country}
                        onChange={(itemValue, itemIndex) =>
                          this.UpdateState(itemValue.target.value, "country")
                        }
                      >
                        <option value="0">Select country </option>
                        {this.state.Get_country.map((country, Index) => {
                          return (
                            <option
                              label={country.name}
                              value={country.slug}
                              key={Index}
                            />
                          );
                        })}
                      </select>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.state}
                      onChange={(e) => this.setState({ state: e.target.value })}
                    >
                      <select
                        className="dropdown"
                        selectedvalue={this.state.state}
                        onChange={(itemValue, itemIndex) =>
                          this.UpdateState(itemValue.target.value, "state")
                        }
                      >
                        <option value="0">Select State</option>
                        {this.state.Get_state.map((state, Index) => {
                          return (
                            <option
                              label={state.name}
                              value={state.slug}
                              key={Index}
                            />
                          );
                        })}
                      </select>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.city}
                      onChange={(e) => this.setState({ city: e.target.value })}
                    >
                      <select
                        className="dropdown"
                        selectedvalue={this.state.city}
                        onChange={(itemValue, itemIndex) =>
                          this.UpdateState(itemValue.target.value, "city")
                        }
                      >
                        <option value="0">Select City</option>
                        {this.state.Get_city.map((city, Index) => {
                          return (
                            <option
                              label={city.name}
                              value={city.slug}
                              key={Index}
                            />
                          );
                        })}
                      </select>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.zipcode}
                      onChange={(e) =>
                        this.setState({ zipcode: e.target.value })
                      }
                    >
                      <select
                        className="dropdown"
                        selectedvalue={this.state.zipcode}
                        onChange={(itemValue, itemIndex) =>
                          this.UpdateState(itemValue.target.value, "zipcode")
                        }
                      >
                        <option value="0">Select zipcode </option>
                        {this.state.Get_zipcode.map((zipcode, Index) => {
                          return (
                            <option
                              label={zipcode.code}
                              value={zipcode.slug}
                              key={Index}
                            />
                          );
                        })}
                      </select>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.landmark}
                      onChange={(e) =>
                        this.setState({ landmark: e.target.value })
                      }
                    >
                      <Input placeholder="Landmark" />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.latitude}
                      onChange={(e) =>
                        this.setState({ latitude: e.target.value })
                      }
                    >
                      <Input placeholder="Latitude" />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item
                      rules={[{ required: true }]}
                      type="text"
                      value={this.state.longitude}
                      onChange={(e) =>
                        this.setState({ longitude: e.target.value })
                      }
                    >
                      <Input placeholder="Longitude" />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item style={{ float: "right" }}>
                      <Button
                        type="primary"
                        onClick={() => this.onSubmitComplex()}
                      >
                        Add
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </Layout>
      </>
    );
  }
}
