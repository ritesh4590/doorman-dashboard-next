import React, { Component } from "react";
import { Breadcrumb, Card } from 'antd';
import '../index.css'
import Layout from '../../components/layout'
import request from '../../config/api'
import Router from "next/router";
var CryptoJS = require("crypto-js");

export default class Dashboard extends React.Component {

	componentDidMount() {
		this._hasComplex();
	}

	_hasComplex = () => {
		request({
			method: "get",
			url: "",
		})
			.then((resp) => {
				// var ciphertext = CryptoJS.AES.encrypt(resp.data.uuid, "process.env.FLEETZI_SECRET");
				// window.localStorage.setItem("uuid", ciphertext.toString());
				window.localStorage.setItem("uuid", resp.meta.uuid);

			})
			.catch((e) => {
				if (e.data.message === "user does not have a complex") {
					Router.push("/create-complex");
				}
				console.log(e);
			});

	}

	render() {
		return (
			<Layout>
				<Breadcrumb style={{ margin: '16px 0' }}>
					<Breadcrumb.Item>Home</Breadcrumb.Item>
					<Breadcrumb.Item>Dashboard</Breadcrumb.Item>
				</Breadcrumb>
				<div className="site-card-border-less-wrapper" style={{ display: 'flex' }}>
					<Card title="Total No of User" bordered={false} style={{ width: 300 }}>
						<div>
							<p>Data</p>
						</div>
					</Card>
					<Card title="Guards" bordered={false} style={{ width: 300, marginLeft: "10px" }}>
						<div>
							<p>Data</p>
						</div>
					</Card>

					<Card title="Tents Issue" bordered={false} style={{ width: 300, marginLeft: "10px" }}>
						<div>
							<p>Data</p>
						</div>
					</Card>

					<Card title="Vehicles" bordered={false} style={{ width: 300, marginLeft: "10px" }}>
						<div>
							<p>Data</p>
						</div>
					</Card>

					<Card title="Check-Ins" bordered={false} style={{ width: 300, marginLeft: "10px" }}>
						<div>
							<p>Data</p>
						</div>
					</Card>
				</div>

				<div className="middle_section" style={{ height: "180px", }}></div>

				<div className="site-card-border-less-wrapper" style={{ display: 'flex', justifyContent: 'space-evenly' }}>
					<Card title="CONVERSATION" bordered={false} style={{ width: 400 }}>
						<div>
							<p><a href="">Create polls</a></p>
							<p>	<a href="">Create Feedback survey</a></p>
							<p>	<a href="">Create Events</a></p>
						</div>
					</Card>

					<Card title="FINANCIAL" bordered={false} style={{ width: 400, marginLeft: "10px" }}>
						<div>
							<p>	<a href="">Dues</a></p>
							<p><a href="">Generate Bills</a></p>
							<p><a href="">Create Event</a></p>
						</div>
					</Card>
				</div>
			</Layout>
		)
	}

}
