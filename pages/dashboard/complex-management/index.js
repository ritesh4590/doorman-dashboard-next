import React from "react";
import {
  Table,
  Tooltip,
  Button,
  Drawer,
  Form,
  Col,
  Row,
  Input,
  Select,
  notification,
  Popconfirm,
  Typography,
  Radio,
  Checkbox,
} from "antd";
import Layout from "../../components/layout/index";
import request from "../../config/api";
import {
  CheckOutlined,
  ExclamationOutlined,
  EditTwoTone,
  DeleteTwoTone,
  PlusOutlined,
} from "@ant-design/icons";
import "./style.css";

const { Option } = Select;
const { Title } = Typography;
var CryptoJS = require("crypto-js");
const text = "Are you sure ?";
const Delete = "Delete";
const edit = "Edit Details";

export default class ComplexManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      phone: "",
      country_code: +91,
      role: "",
      complex_manager: [],
      complex_uuid: "",
      visible: false,
      editDrawerVisible: false,
      search: "",
    };
  }
  componentDidMount() {
    this._getComplexManager();
  }
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  showEditDrawer(record) {
    this.setState({
      editDrawerVisible: true,
      editRecord: record,
    });
  }

  onClose = () => {
    this.setState({
      visible: false,
      editDrawerVisible: false,
    });
  };

  openNotification = (status, message) => {
    notification.open({
      message: status.charAt(0).toUpperCase() + status.substring(1),
      description: message.charAt(0).toUpperCase() + message.substring(1),
      icon:
        status === "success" ? (
          <CheckOutlined style={{ color: "green" }} />
        ) : (
          <ExclamationOutlined style={{ color: "red" }} />
        ),
    });
  };

  _getComplexManager() {
    const uid = window.localStorage.getItem("uuid");
    request({
      method: "get",
      url: "/management/complex/" + uid + "/managers",
    })
      .then((resp) => {
        console.log(resp);
        if (resp.status === "success") {
          this.setState({
            complex_manager: resp.data,
            uuid: resp.data.uuid,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  _addComplexUser = () => {
    const uid = window.localStorage.getItem("uuid");

    let formdata = new FormData();
    formdata.append("first_name", this.state.first_name);
    formdata.append("last_name", this.state.last_name);
    formdata.append("phone", this.state.phone);
    formdata.append("role", this.state.role);
    formdata.append("country_code", "+91");
    request({
      method: "post",
      data: formdata,
      url: "/management/complex/" + uid + "/managers",
    })
      .then((resp) => {
        this.openNotification(resp.status, resp.message);
        // console.log("poiuytrewsdfg-----", resp.data.uid);
        if (resp.status === "success") {
          const { complex_manager } = this.state;
          complex_manager.push(resp.data);
          this.setState({
            complex_manager,
            visible: false,
          });
        }
      })
      .catch((e) => {
        console.log(e);
        this.openNotification(e.data.status, e.data.message);
      });
    // var clearFrom = document.getElementById("clear-form")
    // clearFrom.reset()
    // return false
  };

  clearField() {
    document.chatform.reset();
  }

  _deleteComplexUser(record) {
    const uid = window.localStorage.getItem("uuid");
    request({
      method: "delete",
      url: "/management/complex/" + uid + "/managers/" + record.uid,
    })
      .then((resp) => {
        console.log(resp);
        this.openNotification(resp.status, resp.message);
        this.setState({
          complex_manager: resp.data,
        });
      })
      .catch((e) => {
        console.log(e);
        this.openNotification(e.data.status, e.data.message);
      });
  }

  render() {
    const tableData = this.state.complex_manager.map((complex, key) => ({
      role: complex.role_verbose,
      users: complex.user.first_name + " " + complex.user.last_name,
      phone: complex.user.phone,
      uid: complex.uuid,
    }));

    const columns = [
      {
        title: "Full Name",
        dataIndex: "users",
        key: "users",
      },

      {
        key: "role",
        title: "Role",
        dataIndex: "role",
      },
      {
        key: "phone",
        title: "Phone",
        dataIndex: "phone",
      },
      {
        title: "Actions",
        dataIndex: "uid",
        key: "uid",
        render: (uid, record) => (
          <>
            <Tooltip placement="left" title={edit}>
              <EditTwoTone
                onClick={() => {
                  this.showEditDrawer(record);
                }}
                style={{
                  marginLeft: "3px",
                  marginRight: "8px",
                }}
              />
            </Tooltip>
            <Popconfirm
              placement="top"
              title={text}
              onConfirm={() => {
                this._deleteComplexUser(record);
              }}
              okText="Yes"
              cancelText="No"
            >
              <Tooltip placement="right" title={Delete}>
                <DeleteTwoTone />
              </Tooltip>
            </Popconfirm>
          </>
        ),
      },
    ];

    return (
      <>
        <Layout>
          <Row gutter={24} style={{ alignItems: "center" }}>
            <Col span={15}>
              <Title level={4}>Complex Users</Title>
            </Col>
            <Col span={3}>
              <Checkbox>Admin</Checkbox>
            </Col>
            <Col span={3}>
              <Checkbox>Guard</Checkbox>
            </Col>

            <Col span={3}>
              <Button key="1" type="primary" onClick={this.showDrawer}>
                Add user
              </Button>
            </Col>
          </Row>
          <br />
          <Table
            columns={columns}
            dataSource={tableData}
            rowKey="role"
            onChange={this.handleTableChange}
          />
          <div>
            <Drawer
              title="Create a new user"
              width={370}
              onClose={this.onClose}
              visible={this.state.visible}
              bodyStyle={{ paddingBottom: 80 }}
              footer={
                <div
                  style={{
                    textAlign: "right",
                  }}
                >
                  <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                    Cancel
                  </Button>
                  <Button onClick={this._addComplexUser} type="primary">
                    Submit
                  </Button>
                </div>
              }
            >
              <Form layout="vertical" id="clear-form" onClick="clearField();">
                {/* <Row gutter={16}> */}
                <Col span={24}>
                  <Form.Item
                    label="First Name"
                    value={this.state.first_name}
                    onChange={(e) =>
                      this.setState({ first_name: e.target.value })
                    }
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="First name" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    label="Last Name"
                    value={this.state.last_name}
                    onChange={(e) =>
                      this.setState({ last_name: e.target.value })
                    }
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="Last Name" />
                  </Form.Item>
                </Col>
                {/* </Row> */}
                <Col span={24}>
                  <Form.Item
                    label="Phone"
                    value={this.state.phone}
                    onChange={(e) => this.setState({ phone: e.target.value })}
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="Phone No." />
                  </Form.Item>
                </Col>

                <Col span={24}>
                  <Form.Item
                    label="Role"
                    value={this.state.role}
                    rules={[{ required: true }]}
                    selectedvalue={this.state.role}
                    onChange={(e) => this.setState({ role: e.target.value })}
                  >
                    <select>
                      <option label="Role Type" />
                      <option value="ad">Admin</option>
                      <option value="gu">Guard</option>
                    </select>
                  </Form.Item>
                </Col>
              </Form>
            </Drawer>
          </div>

          <div>
            <Drawer
              title="Edit user"
              width={370}
              onClose={this.onClose}
              visible={this.state.editDrawerVisible}
              bodyStyle={{ paddingBottom: 80 }}
              footer={
                <div
                  style={{
                    textAlign: "right",
                  }}
                >
                  <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                    Cancel
                  </Button>
                  <Button type="primary">Submit</Button>
                </div>
              }
            >
              <Form layout="vertical" hideRequiredMark>
                {/* <Row gutter={16}> */}
                <Col span={24}>
                  <Form.Item
                    label="First Name"
                    value={this.state.first_name}
                    onChange={(e) =>
                      this.setState({ first_name: e.target.value })
                    }
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="First name" />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    label="Last Name"
                    value={this.state.last_name}
                    onChange={(e) =>
                      this.setState({ last_name: e.target.value })
                    }
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="Last Name" />
                  </Form.Item>
                </Col>
                {/* </Row> */}
                <Col span={24}>
                  <Form.Item
                    label="Phone"
                    value={this.state.phone}
                    onChange={(e) => this.setState({ phone: e.target.value })}
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="Phone No." />
                  </Form.Item>
                </Col>

                <Col span={24}>
                  <Form.Item
                    label="Role"
                    value={this.state.role}
                    rules={[{ required: true }]}
                    selectedvalue={this.state.role}
                    onChange={(e) => this.setState({ role: e.target.value })}
                  >
                    <select>
                      <option label="Role Type" />
                      <option value="ad">Admin</option>
                      <option value="gu">Guard</option>
                    </select>
                  </Form.Item>
                </Col>
              </Form>
            </Drawer>
          </div>
        </Layout>
      </>
    );
  }
}
