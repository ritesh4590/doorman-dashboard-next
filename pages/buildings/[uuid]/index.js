import React, { Component } from "react";
import Layout from "../../../components/layout/";
import { ResidenceDetailLink } from "../../../config/links";
import { BuildingLink } from "../../../config/links";
import Link from "next/link";
import request from "../../../config/api";
import {
  Table,
  Tooltip,
  Button,
  Drawer,
  Form,
  Row,
  Col,
  Input,
  notification,
  Typography,
  Tabs,
  Breadcrumb,
  Divider,
} from "antd";
import {
  CheckOutlined,
  ExclamationOutlined,
  IdcardOutlined,
  EditTwoTone,
} from "@ant-design/icons";
const { Search } = Input;
const details = "View Details";
const edit = "Edit Details";
class BuildingInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone_no: "",
      gender: "",
      floor_no: "",
      address_line_1: "",
      address_line_2: "",
      job: "",
      value: 1,
      residence: [],
      uuid: this.props.uuid,
      visible: false,
      editDrawerVisible: false,
      addResidenceDrawer: false,
      uuid: this.props.uuid,
      search: "",
    };
  }

  componentDidMount() {
    this._getResidence();
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
      editDrawerVisible: false,
    });
  };

  showEditDrawer = () => {
    this.setState({
      editDrawerVisible: true,
    });
  };

  openNotification = (status, message) => {
    notification.open({
      message: status.charAt(0).toUpperCase() + status.substring(1),
      description: message.charAt(0).toUpperCase() + message.substring(1),
      icon:
        status === "success" ? (
          <CheckOutlined style={{ color: "green" }} />
        ) : (
            <ExclamationOutlined style={{ color: "red" }} />
          ),
    });
  };

  onChange = (e) => {
    this.setState({
      value: e.target.value,
    });
  };

  _getResidence() {
    const uid = window.localStorage.getItem("uuid");
    request({
      method: "get",
      url:
        "/management/complex/" +
        uid +
        "/buildings/" +
        this.props.uuid +
        "/residences",
    })
      .then((resp) => {
        if (resp.status === "success") {
          this.setState({
            residence: resp.data,
            uuid: resp.data.uuid,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  _addResidence = () => {
    const uid = window.localStorage.getItem("uuid");
    let formdata = new FormData();
    formdata.append("name", this.state.name);
    formdata.append("email", this.state.email);
    formdata.append("phone_no", this.state.phone_no);
    formdata.append("gender", this.state.gender);
    formdata.append("floor_no", this.state.floor_no);
    formdata.append("address_line_1", this.state.address_line_1);
    formdata.append("address_line_2", this.state.address_line_2);
    formdata.append("job", this.state.job);
    request({
      method: "post",
      data: formdata,
      url:
        "/management/complex/" +
        uid +
        "/buildings/" +
        this.props.uuid +
        "/residences",
    })
      .then((resp) => {
        console.log(resp);
        this.openNotification(resp.status, resp.message);
        if (resp.status === "success") {
          const { residence } = this.state;
          residence.push(resp.data);
          this.setState({
            residence,
            visible: false,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  updateSearch(event) {
    this.setState({
      search: event.target.value.substr(0, 20),
    });
  }

  render() {
    let filterData = this.state.residence.filter((residences) => {
      return (
        residences.address_line_1.toLowerCase().indexOf(this.state.search.toLowerCase()) !==
        -1
      );
    });
    const tableData =  filterData.map((resident, key) => ({
      uuid: resident.uuid,
      Address_Line_1: resident.address_line_1,
      Address_Line_2: resident.address_line_2,
    }));

    const columns = [
      {
        title: "Address_Line_1",
        dataIndex: "Address_Line_1",
        key: "Address_Line_1",
      },
      {
        title: "Address_Line_2",
        dataIndex: "Address_Line_2",
        key: "Address_Line_2",
      },
      {
        title: "Actions",
        dataIndex: "uuid",
        key: "uuid",
        render: (uuid, record) => (
          <>
           <Tooltip placement="top" title={edit}>
            <EditTwoTone
              onClick={() => {
                this.showEditDrawer(record);
              }}
              style={{
                marginLeft: "3px",
                marginRight: "8px",
              }}
            />
            </Tooltip>
            <ResidenceDetailLink
              uuid={this.props.uuid}
              residence_uuid={record.uuid}
            >
              <Tooltip placement="top" title={details}>
                <IdcardOutlined style={{ color: "#1890ff" }} />
              </Tooltip>
            </ResidenceDetailLink>
          </>
        ),
      },
    ];

    return (
      <div>
        <Row style={{ paddingBottom: "20px" }} gutter={24}>
          <Col span={12}>
            <Search
              placeholder="search"
              value={this.state.search}
              onChange={this.updateSearch.bind(this)}
              style={{ width: 200 }}
            />
          </Col>
          <Col span={12} >
            <Button type="primary" onClick={this.showDrawer} style={{ float: "right" }}>
              Add Residence
          </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Table columns={columns} dataSource={tableData} rowKey="uuid" />
        <Drawer
          title="Add Residence"
          width={370}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: "right",
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Cancel
              </Button>
              <Button onClick={this._addResidence} type="primary">
                Submit
              </Button>
            </div>
          }
        >
          <Form layout="vertical" name="dynamic_form_item">
            <Col span={24}>
              <Form.Item
                label="Address Line 1"
                value={this.state.address_line_1}
                onChange={(e) =>
                  this.setState({ address_line_1: e.target.value })
                }
                rules={[{ required: true }]}
              >
                <Input placeholder="Address Line 1" />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Form.Item
                label="Address Line 2"
                value={this.state.address_line_2}
                onChange={(e) =>
                  this.setState({ address_line_2: e.target.value })
                }
                rules={[{ required: true }]}
              >
                <Input placeholder="Address Line 2" />
              </Form.Item>
            </Col>
          </Form>
        </Drawer>
        {/* edit details */}
        <Drawer
          title="Edit Details"
          width={370}
          onClose={this.onClose}
          visible={this.state.editDrawerVisible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: "right",
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Cancel
              </Button>
              <Button onClick={this.onClose} type="primary">
                Submit
              </Button>
            </div>
          }
        >
          <Form layout="vertical" name="dynamic_form_item">
            <Col span={24}>
              <Form.Item
                label="Address Line 1"
                value={this.state.address_line_1}
                onChange={(e) =>
                  this.setState({ address_line_1: e.target.value })
                }
                rules={[{ required: true }]}
              >
                <Input placeholder="Address Line 1" />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Form.Item
                label="Address Line 2"
                value={this.state.address_line_2}
                onChange={(e) =>
                  this.setState({ address_line_2: e.target.value })
                }
                rules={[{ required: true }]}
              >
                <Input placeholder="Address Line 2" />
              </Form.Item>
            </Col>
          </Form>
        </Drawer>
      </div>
    );
  }
}

const BuildingDetail = ({ uuid }) => {
  return (
    <Layout>
      <Row>
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>
            <BuildingLink>
              <a>Buildings</a>
            </BuildingLink>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a>Residences</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </Row>
      <Divider style={{ background: "rgba(0,0,0,0.1)" }}></Divider>
      <BuildingInfo uuid={uuid} />
    </Layout>
  );
};

BuildingDetail.getInitialProps = async ({ query }) => {
  const { uuid } = query;
  return { uuid };
};
export default BuildingDetail;
