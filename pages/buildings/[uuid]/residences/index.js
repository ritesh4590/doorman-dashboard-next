import React from "react";
import {
  Table,
  Tooltip,
  Button,
  Drawer,
  Form,
  Col,
  Input,
  Select,
  notification,
  Typography,
  Tabs,
  Radio,
} from "antd";
import Layout from "../../../../components/layout/index";
import request from "../../../../config/api";
import { ResidenceDetailLink } from "../../../../config/links";
import {
  CheckOutlined,
  ExclamationOutlined,
  MinusCircleOutlined,
  PlusOutlined,
  UserOutlined,
  EditTwoTone,
  AndroidOutlined,
} from "@ant-design/icons";
const { TabPane } = Tabs;
const { Title } = Typography;
var CryptoJS = require("crypto-js");
const text = "Are you sure ?";
const Delete = "Delete";
const edit = "Edit Details";

class AddBuilding extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone_no: "",
      gender: "",
      floor_no: "",
      address_line_1: "",
      address_line_2: "",
      job: "",
      value: 1,
      residence: [],
      uuid: this.props.uuid,
      visible: false,
      editDrawerVisible: false,
      addResidenceDrawer: false,
    };
  }
  componentDidMount() {
    this._getResidence();
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
      editDrawerVisible: false,
      addResidenceDrawer: false,
    });
  };

  showEditDrawer = () => {
    this.setState({
      editDrawerVisible: true,
    });
  };

  showResidenceDrawer = () => {
    this.setState({
      addResidenceDrawer: true,
    });
  };

  openNotification = (status, message) => {
    notification.open({
      message: status.charAt(0).toUpperCase() + status.substring(1),
      description: message.charAt(0).toUpperCase() + message.substring(1),
      icon:
        status === "success" ? (
          <CheckOutlined style={{ color: "green" }} />
        ) : (
          <ExclamationOutlined style={{ color: "red" }} />
        ),
    });
  };

  onChange = (e) => {
    console.log("radio checked", e.target.value);
    this.setState({
      value: e.target.value,
    });
  };

  _getResidence() {
    const uid = window.localStorage.getItem("uuid");
    request({
      method: "get",
      url: "/management/complex/" + uid + "/buildings/" + this.props.uuid +"/residences",
    })
      .then((resp) => {
        console.log("Residence Uuid-->>",resp)
        if (resp.status === "success") {
          this.setState({
            residence: resp.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  _addResidence = () => {
    const uid = window.localStorage.getItem("uuid");
    let formdata = new FormData();
    formdata.append("name", this.state.name);
    formdata.append("email", this.state.email);
    formdata.append("phone_no", this.state.phone_no);
    formdata.append("gender", this.state.gender);
    formdata.append("floor_no", this.state.floor_no);
    formdata.append("address_line_1", this.state.address_line_1);
    formdata.append("address_line_2", this.state.address_line_2);
    formdata.append("job", this.state.job);
    request({
      method: "post",
      data: formdata,
      url: "/management/complex/" + uid + "/buildings/" + this.props.uuid +"/residences",
    })
      .then((resp) => {
        console.log(resp)
        this.openNotification(resp.status, resp.message);
        if (resp.status === "success") {
          const { residence } = this.state;
          residence.push(resp.data);
          this.setState({
            residence,
            addResidenceDrawer: false,
          });
        }
      })
      .catch((e) => {
        console.log(e);
        this.openNotification(e.data.status, e.data.message);
      });
  };

  render() {
    const tableData = this.state.residence.map((resident, key) => ({
      uuid:resident.uuid,
    	Address_Line_1: resident.address_line_1,
    	Address_Line_2: resident.address_line_2,
    	// uid: resident.uuid,
    }));

    const columns = [
      {
    		title: "uuid",
    		dataIndex: "uuid",
    		key: "uuid",
        render: (uuid, record) => (
          <>
            <ResidenceDetailLink uuid={this.props.uuid}>
              <a>{record.uuid}</a>
            </ResidenceDetailLink>
          </>
        )
      },
      {
    		title: "Address_Line_1",
    		dataIndex: "Address_Line_1",
    		key: "Address_Line_1",
      },
      {
    		title: "Address_Line_2",
    		dataIndex: "Address_Line_2",
    		key: "Address_Line_2",
    	},
    	{
    		title: "Actions",
    		dataIndex: "uid",
    		key: "uid",
    		render: (uid, record) => (
    			<>
    				{/* <Tooltip placement="top" title={edit}> */}
    					<EditTwoTone
    						onClick={() => {
    							this.showEditDrawer(record);
    						}}
    						style={{
    							marginLeft: "3px",
    							marginRight: "8px",
    						}}
    					/>
    				{/* </Tooltip> */}
    				{/* // <Tooltip placement="top" title={residence}> */}
    					{/* <UserAddOutlined /> */}
    				{/* </Tooltip> */}
    			</>
    		),
    	},
    ]
    const formItemLayout = {
      // labelCol: {
      // 	xs: { span: 24 },
      // 	sm: { span: 4 },
      // },
      // wrapperCol: {
      // 	xs: { span: 24 },
      // 	sm: { span: 20 },
      // },
    };
    const formItemLayoutWithOutLabel = {
      // wrapperCol: {
      // 	xs: { span: 24, offset: 0 },
      // 	sm: { span: 20, offset: 4 },
      // },
    };

    const DynamicFieldSet = () => {
      const onFinish = (values) => {
        console.log("Received values of form:", values);
      };
    };
    const radioStyle = {
      display: "block",
      height: "30px",
      lineHeight: "30px",
    };
    const { value } = this.state;
    return (
      <>
        <Tabs defaultActiveKey="1">
          <TabPane
            tab={
              <span>
                <UserOutlined />
                Residence
              </span>
            }
            key="1"
          >
            <Col span={3} style={{ float: "right" }}>
              <Button key="1" type="primary" onClick={this.showResidenceDrawer}>
                Add Residence
              </Button>
            </Col>
          </TabPane>
        </Tabs>

        <br />
        <br />
        <Table
          columns={columns}
          dataSource={tableData}
          rowKey="Address_Line_2"
          onChange={this.handleTableChange}
        />
        <div>
          <Drawer
            title="Add Residence"
            width={370}
            onClose={this.onClose}
            visible={this.state.addResidenceDrawer}
            bodyStyle={{ paddingBottom: 80 }}
            footer={
              <div
                style={{
                  textAlign: "right",
                }}
              >
                <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                  Cancel
                </Button>
                <Button onClick={this._addResidence} type="primary">
                  Submit
                </Button>
              </div>
            }
          >
            <Form
              layout="vertical"
              name="dynamic_form_item"
              {...formItemLayoutWithOutLabel}
            >
              <Col span={24}>
                <Form.Item
                  label="Name"
                  value={this.state.name}
                  onChange={(e) => this.setState({ name: e.target.value })}
                  rules={[{ required: true }]}
                >
                  <Input placeholder="Name" />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label="Email"
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  rules={[{ required: true }]}
                >
                  <Input placeholder="Email" />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label="Phone No."
                  value={this.state.phone_no}
                  onChange={(e) => this.setState({ phone_no: e.target.value })}
                  rules={[{ required: true }]}
                >
                  <Input placeholder="Phone No." />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label="Gender"
                  value={this.state.gender}
                  rules={[{ required: true }]}
                  selectedvalue={this.state.gender}
                  onChange={(e) => this.setState({ gender: e.target.value })}
                >
                  <select>
                    <option label="Gender" />
                    <option value="ad">Male</option>
                    <option value="gu">Female</option>
                  </select>
                </Form.Item>
              </Col>
              <Col span={24}>
                <Radio.Group onChange={this.onChange} value={value}>
                  <label>Marital Status</label>
                  <Radio style={radioStyle} value={1}>
                    Single
                  </Radio>
                  <Radio style={radioStyle} value={4}>
                    Married
                    {value === 4 ? (
                      <>
                        <Form.List name="names">
                          {(fields, { add, remove }) => {
                            return (
                              <div>
                                {fields.map((field, index) => (
                                  <Form.Item
                                    {...(index === 0
                                      ? formItemLayout
                                      : formItemLayoutWithOutLabel)}
                                    label={index === 0 ? "Enter Details" : ""}
                                    required={false}
                                    key={field.key}
                                    style={{ width: "310px" }}
                                  >
                                    <Form.Item
                                      {...field}
                                      validateTrigger={["onChange", "onBlur"]}
                                      rules={[
                                        {
                                          required: true,
                                          whitespace: true,
                                          message:
                                            "Please input passenger's name or delete this field.",
                                        },
                                      ]}
                                      noStyle
                                    >
                                      <div
                                        style={{
                                          boxShadow: "0px 0px 14px #eeeeee",
                                          padding: "15px",
                                        }}
                                      >
                                        <Input
                                          placeholder="Name"
                                          style={{ width: "100%" }}
                                        />
                                        <br />
                                        <br />
                                        <Input
                                          placeholder="Age"
                                          style={{ width: "100%" }}
                                        />
                                        <br />
                                        <br />
                                        <Input
                                          placeholder="Relation"
                                          style={{ width: "100%" }}
                                        />
                                      </div>
                                    </Form.Item>
                                    <Tooltip placement="top" title={Delete}>
                                      {fields.length > 0 ? (
                                        <MinusCircleOutlined
                                          className="dynamic-delete-button"
                                          style={{
                                            position: "absolute",
                                            top: -5,
                                            right: -5,
                                          }}
                                          onClick={() => {
                                            remove(field.name);
                                          }}
                                        />
                                      ) : null}
                                    </Tooltip>
                                  </Form.Item>
                                ))}

                                <Form.Item style={{ paddingTop: "10px" }}>
                                  <Button
                                    type="dashed"
                                    onClick={() => {
                                      add();
                                    }}
                                    style={{ width: "100%" }}
                                  >
                                    <PlusOutlined /> Add Family Member
                                  </Button>
                                </Form.Item>
                              </div>
                            );
                          }}
                        </Form.List>
                      </>
                    ) : null}
                  </Radio>
                </Radio.Group>
              </Col>
              <Col span={24}>
                <Form.Item
                  label="Floor No."
                  value={this.state.floor_no}
                  onChange={(e) => this.setState({ floor_no: e.target.value })}
                  rules={[{ required: true }]}
                >
                  <Input placeholder="Floor No." />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label="Address Line 1"
                  value={this.state.address_line_1}
                  onChange={(e) =>
                    this.setState({ address_line_1: e.target.value })
                  }
                  rules={[{ required: true }]}
                >
                  <Input placeholder="Address Line 1" />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label="Address Line 2"
                  value={this.state.address_line_2}
                  onChange={(e) =>
                    this.setState({ address_line_2: e.target.value })
                  }
                  rules={[{ required: true }]}
                >
                  <Input placeholder="Address Line 2" />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label="JOB"
                  value={this.state.job}
                  rules={[{ required: true }]}
                  selectedvalue={this.state.job}
                  onChange={(e) => this.setState({ job: e.target.value })}
                >
                  <select>
                    <option label="Job" />
                    <option value="gu">Business</option>
                    <option value="ad">Service</option>
                  </select>
                </Form.Item>
              </Col>
            </Form>
          </Drawer>
        </div>
      </>
    );
  }
}

const ResidenceDetail = ({ uuid }) => {
  return (
    <Layout>
      <AddBuilding uuid={uuid} />
    </Layout>
  );
};

ResidenceDetail.getInitialProps = async ({ query }) => {
  const { uuid } = query;
  return { uuid };
};
export default ResidenceDetail;
