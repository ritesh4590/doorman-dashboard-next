import React, { Component } from "react";
import Layout from "../../../../components/layout/";
import request from "../../../../config/api";
import { BuildingDetailLink, BuildingLink } from "../../../../config/links";
import { Table, Breadcrumb, Divider, Row } from "antd";

class Residenceinfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collectDetails: {},
    };
  }

  componentDidMount() {
    this._getResidenceDetails();
  }

  _getResidenceDetails = () => {
    const uid = window.localStorage.getItem("uuid");
    request({
      method: "get",
      url:
        "/management/complex/" +
        uid +
        "/buildings/" +
        this.props.uuid +
        "/residences/" +
        this.props.residence_uuid,
    })
      .then((resp) => {
        if (resp.status === "success") {
          this.setState({
            collectDetails: resp.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };
  render() {
    const data = [
      {
        key: "1",
        Address_Line_1: this.state.collectDetails.address_line_1,
        Address_Line_2: this.state.collectDetails.address_line_2,
      },
    ];
    const columns = [
      {
        title: "Address_Line_1",
        dataIndex: "Address_Line_1",
        key: "Address_Line_1",
      },
      {
        title: "Address_Line_2",
        dataIndex: "Address_Line_2",
        key: "Address_Line_2",
      },
    ];
    return (
      <>
        <Table columns={columns} dataSource={data} />
      </>
    );
  }
}

const ResidentDetail = ({ uuid, residence_uuid }) => {
  return (
    <Layout>
      <Row>
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>
            <BuildingLink>
              <a>Buildings</a>
            </BuildingLink>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <BuildingDetailLink uuid={uuid}>
              <a>Residences</a>
            </BuildingDetailLink>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a>Resident</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </Row>
      <Divider style={{ background: "rgba(0,0,0,0.1)" }}></Divider>
      <Residenceinfo uuid={uuid} residence_uuid={residence_uuid} />
    </Layout>
  );
};

ResidentDetail.getInitialProps = async ({ query }) => {
  const { uuid } = query;
  const { residence_uuid } = query;
  return { uuid, residence_uuid };
};
export default ResidentDetail;
