import React from "react";
import Layout from "../../components/layout/index";
import request from "../../config/api";
import Link from "next/link";
import { BuildingDetailLink } from "../../config/links";
import {
  Table,
  Button,
  Drawer,
  Form,
  Col,
  Row,
  Input,
  notification,
  Breadcrumb,
  Divider,
} from "antd";
import {
  CheckOutlined,
  ExclamationOutlined,
} from "@ant-design/icons";
const { Search } = Input;


export default class AddBuilding extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      buildings: [],
      complex_uuid: "",
      visible: false,
      search: "",
    };
  }
  componentDidMount() {
    this._getComplexManager();
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
      
    });
  };

  openNotification = (status, message) => {
    notification.open({
      message: status.charAt(0).toUpperCase() + status.substring(1),
      description: message.charAt(0).toUpperCase() + message.substring(1),
      icon:
        status === "success" ? (
          <CheckOutlined style={{ color: "green" }} />
        ) : (
            <ExclamationOutlined style={{ color: "red" }} />
          ),
    });
  };

  _getComplexManager() {
    const uid = window.localStorage.getItem("uuid");
    request({
      method: "get",
      url: "/management/complex/" + uid + "/buildings",
    })
      .then((resp) => {
        if (resp.status === "success") {
          this.setState({
            buildings: resp.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  _addBuilding = () => {
    const uid = window.localStorage.getItem("uuid");
    let formdata = new FormData();
    formdata.append("name", this.state.name);
    request({
      method: "post",
      data: formdata,
      url: "/management/complex/" + uid + "/buildings",
    })
      .then((resp) => {
        this.openNotification(resp.status, resp.message);
        if (resp.status === "success") {
          const { buildings } = this.state;
          buildings.push(resp.data);
          this.setState({
            buildings,
            visible: false,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  updateSearch(event) {
    this.setState({
      search: event.target.value.substr(0, 20),
    });
  }

  render() {
    let filterData = this.state.buildings.filter((building) => {
      return (
        building.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !==
        -1
      );
    });
    const tableData = filterData.map((building, key) => ({
      name: building.name,
      uuid: building.uuid,
    }));

    const columns = [
      {
        title: "Building Name",
        dataIndex: "uuid",
        key: "uuid",
        render: (uuid, record) => (
          <BuildingDetailLink uuid={uuid}>
            <a>{record.name}</a>
          </BuildingDetailLink>
        ),
      },
    ];

    return (
      <>
        <Layout>
          <Row>
            <Col span={21}>
              <Breadcrumb>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>
                  <a>Buildings</a>
                </Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Divider style={{ background: "rgba(0,0,0,0.1)" }}></Divider>

          <Row style={{ paddingBottom: "20px" }} gutter={24}>
            <Col span={12}>
              <Search
                placeholder="search"
                value={this.state.search}
                onChange={this.updateSearch.bind(this)}
                style={{ width: 200 }}
              />
            </Col>
            <Col span={12}>
              <Button key="1" type="primary" onClick={this.showDrawer} style={{ float: "right" }}>
                Add Building
              </Button>
            </Col>
          </Row>

          <Table
            columns={columns}
            dataSource={tableData}
            rowKey="role"
            onChange={this.handleTableChange}
            rowKey="name"
          />
          <div>
            <Drawer
              title="Add Building"
              width={370}
              onClose={this.onClose}
              visible={this.state.visible}
              bodyStyle={{ paddingBottom: 80 }}
              footer={
                <div
                  style={{
                    textAlign: "right",
                  }}
                >
                  <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                    Cancel
                  </Button>
                  <Button onClick={this._addBuilding} type="primary">
                    Submit
                  </Button>
                </div>
              }
            >
              <Form layout="vertical">
                <Col span={24}>
                  <Form.Item
                    label="Building Name"
                    value={this.state.name}
                    onChange={(e) => this.setState({ name: e.target.value })}
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="Building Name" />
                  </Form.Item>
                </Col>
              </Form>
            </Drawer>
          </div>
        </Layout>
      </>
    );
  }
}
