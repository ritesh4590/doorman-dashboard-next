import React, { Component } from "react";
import './index.css'
import { Button } from "antd";
import Layout from '../../components/layout/index'

export default class Adminland extends Component {
	constructor(props) {
		super(props);
		this.state = {}
	}
	render() {
		return (
			<>
				<Layout >
					<div className="adminland-main-wrapper">
						<header>
							<div className="adminland-heading">
								{/* <div>ADMIN LAND</div> */}
								<br />
								<span>Manage Your Doorman account</span>
							</div>
						</header>
						<div className="bodywrapper">
							<div className="RM-detail">
								<div className="owner">Account Owner</div>
								<div className="owner-oath">Random dude aka some one will pay or this system</div><br />
								<p className="owner">You are account owner so you can...</p>
								<p><a>Add/Remove Administrator</a></p>
								<p><a>Handle Building info & invoice</a></p>
								<p><a>Cancel Account</a></p>
								<p><a>Support Ticket</a></p>
							</div>
							<div className="RM-contact">
								<div className="contact-info">
								</div>
							</div>
						</div>
						<div className="border-bottom"></div>
						<div className="bodywrapper">
							<div className="RM-detail">
								<div className="owner">Administrator</div>
								<div className="owner-oath">Name of the Admin</div><br />
								<p className="owner">You are an Admin so you can...</p>
								<p><a>Add/Remove/Delete User</a></p>
								<p><a>Add/Remove/Guards/Househelp User</a></p>
								<p><a>Create Polls & Survey</a></p>
								<p><a>Create Polls & Survey</a></p>
							</div>
							<div className="RM-contact">
								<div className="contact-info">
								</div>
							</div>
						</div>
					</div>
				</Layout>
			</>
		)
	}
}    